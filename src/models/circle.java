package models;

public class circle {
  private double radius = 1.0;

  public circle(double radius) {
    this.radius = radius;
  }

  public double getRadius() {
    return radius;
  }

  public void setRadius(double radius) {
    this.radius = radius;
  }

  public circle() {
  }

  public double getArea(){
    return Math.pow(this.radius, 2)*Math.PI;
  }

  public double getCircumference(){
    return 2*Math.PI*this.radius;
  }

  public String toString(){
    return "Circle[radius=" + this.radius + "]";
  }
  
}
